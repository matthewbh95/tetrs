use sim::TetrisSim;
use grid::SetPieceResult;
use piece::Tetromino;

pub struct Key {
    pressed: bool,

    delay_counter: f32,
    repeat_delay: f32, // seconds before repeats start

    repeat_rate: f32, // repeats per second

    buffered_presses: f32,
}

impl Key {
    fn new(repeat_delay: f32, repeat_rate: f32) -> Key {
        return Key {
            pressed: false,
            delay_counter: 0.0,
            repeat_delay,
            repeat_rate,
            buffered_presses: 0.0,
        };
    }

    pub fn down_event(&mut self) {
        if self.pressed {
            return;
        }
        self.pressed = true;
        self.delay_counter = 0.0;
        self.buffered_presses = 1.0;
    }

    pub fn up_event(&mut self) {
        if !self.pressed {
            return;
        }
        self.pressed = false;
        self.delay_counter = 0.0;
        self.buffered_presses = 0.0;
    }

    fn poll_and_consume(&mut self) -> bool {
        if self.buffered_presses >= 1.0 {
            self.buffered_presses -= 1.0;
            return true;
        } else {
            return false;
        }
    }

    fn tick(&mut self) {
        if !self.pressed {
            return;
        }
        if self.delay_counter < self.repeat_delay {
            self.delay_counter += 1.0 / 60.0;
        } else if self.buffered_presses < 1.0 {
            self.buffered_presses += self.repeat_rate * (1.0 / 60.0);
            if self.buffered_presses > 1.0 {
                self.buffered_presses = 1.0;
            }
        }
    }
}

pub struct Input {
    pub up: Key,
    pub down: Key,
    pub left: Key,
    pub right: Key,

    pub rotate_cw: Key,
    pub rotate_ccw: Key,

    pub hard_drop: Key,

    pub hold: Key,
}

impl Input {
    fn new() -> Input {
        return Input {
            up: Key::new(0.0, 0.0),
            down: Key::new(0.3, 60.0),
            left: Key::new(0.3, 60.0),
            right: Key::new(0.3, 60.0),
            rotate_cw: Key::new(0.0, 0.0),
            rotate_ccw: Key::new(0.0, 0.0),
            hard_drop: Key::new(0.0, 0.0),
            hold: Key::new(0.0, 0.0),
        };
    }
}

impl Input {
    fn tick(&mut self) {
        self.up.tick();
        self.down.tick();
        self.left.tick();
        self.right.tick();

        self.rotate_cw.tick();
        self.rotate_ccw.tick();

        self.hard_drop.tick();

        self.hold.tick();
    }
}

pub struct TetrisGame {
    pub is_running: bool,
    pub sim: TetrisSim,
    gravity_rate: f32,
    gravity_counter: f32,
    lock_timer: f32,
    can_hold: bool,
    pub input: Input,

}

impl TetrisGame {
    pub fn new() -> TetrisGame {
        return TetrisGame {
            is_running: true,
            sim: TetrisSim::new(),
            gravity_rate: 1.0 / 60.0,
            gravity_counter: 0.0,
            lock_timer: 0.5,
            can_hold: true,
            input: Input::new(),
        };
    }

    fn reset_lock(&mut self) {
        self.lock_timer = 0.5;
    }

    pub fn tick(&mut self) {
        self.input.tick();

        if self.input.hard_drop.poll_and_consume() {
            while self.sim.move_piece_down() {}
            self.lock();
            return;
        }

        if self.input.left.poll_and_consume() {
            if self.sim.move_piece_left() {
//                self.reset_lock();
            }
        }

        if self.input.right.poll_and_consume() {
            if self.sim.move_piece_right() {
//                self.reset_lock();
            }
        }

        if self.input.rotate_cw.poll_and_consume() {
            self.sim.rotate_cw();
        }

        if self.input.rotate_ccw.poll_and_consume() {
            self.sim.rotate_ccw();
        }

        if self.input.down.poll_and_consume() {
            self.gravity_counter += 1.0;
        }


        if self.input.hold.poll_and_consume() && self.can_hold {
            self.sim.hold();
            self.can_hold = false;
            self.reset_lock();
            self.gravity_counter = 0.0;
        }

        self.gravity_counter += self.gravity_rate;

        while self.gravity_counter > 1.0 {
            if self.sim.move_piece_down() {
                self.gravity_counter -= 1.0;
                self.reset_lock();
            } else {
                self.gravity_counter = 0.0;
            }
        }

        if self.sim.is_piece_on_ground() {
            self.lock_timer -= (1.0 / 60.0);
        }

        if self.lock_timer <= 0.0 {
            self.lock();
        }
    }

    fn lock(&mut self) {
        let result = self.sim.lock_piece();
        match result {
            SetPieceResult::Invalid => { self.is_running = false; }
            _ => {}
        };
        self.can_hold = true;
        self.reset_lock();
    }
}