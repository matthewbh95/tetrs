use piece::Tetromino;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum TetrominoType {
    I,
    J,
    L,
    O,
    S,
    T,
    Z,
}

static I_BLOCKS: [(i32, i32); 4] = [(0, -1), (0, 0), (0, 1), (0, 2)];
static J_BLOCKS: [(i32, i32); 4] = [(1, -1), (0, -1), (0, 0), (0, 1)];
static L_BLOCKS: [(i32, i32); 4] = [(1, 1), (0, -1), (0, 0), (0, 1)];
static O_BLOCKS: [(i32, i32); 4] = [(1, 0), (1, 1), (0, 0), (0, 1)];
static S_BLOCKS: [(i32, i32); 4] = [(1, 0), (1, 1), (0, -1), (0, 0)];
static T_BLOCKS: [(i32, i32); 4] = [(1, 0), (0, -1), (0, 0), (0, 1)];
static Z_BLOCKS: [(i32, i32); 4] = [(1, -1), (1, 0), (0, 0), (0, 1)];

static JLSTZ_KICK_OFFSETS: [[(i32, i32); 5]; 4] = [
    [(0, 0), (0, 0), (0, 0), (0, 0), (0, 0)],
    [(0, 0), (0, 1), (-1, 1), (2, 0), (2, 1)],
    [(0, 0), (0, 0), (0, 0), (0, 0), (0, 0)],
    [(0, 0), (0, -1), (-1, -1), (2, 0), (2, -1)]];

static I_KICK_OFFSETS: [[(i32, i32); 5]; 4] = [
    [(0, 0), (0, -1), (0, 2), (0, -1), (0, 2)],
    [(0, -1), (0, 0), (0, 0), (1, 0), (-2, 0)],
    [(1, -1), (1, 1), (1, -2), (0, 1), (0, -2)],
    [(1, 0), (1, 0), (1, 0), (-1, 0), (2, 0)],
];
static O_KICK_OFFSETS: [[(i32, i32); 5]; 4] = [
    [(0, 0), (0, 0), (0, 0), (0, 0), (0, 0)],
    [(-1, 0), (0, 0), (0, 0), (0, 0), (0, 0)],
    [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 0)],
    [(0, -1), (0, 0), (0, 0), (0, 0), (0, 0)],
];

impl TetrominoType {
    pub fn spawn(&self, row: i32, col: i32) -> Tetromino {
        return Tetromino {
            row,
            col,
            orientation: 0,
            tetromino_type: *self,
        };
    }

    pub fn color(&self) -> u32 {
        return match *self {
            TetrominoType::I => 0,
            TetrominoType::J => 1,
            TetrominoType::L => 2,
            TetrominoType::O => 3,
            TetrominoType::S => 4,
            TetrominoType::T => 5,
            TetrominoType::Z => 6,
        };
    }

    pub fn blocks(&self) -> [(i32, i32); 4] {
        return match *self {
            TetrominoType::I => I_BLOCKS,
            TetrominoType::J => J_BLOCKS,
            TetrominoType::L => L_BLOCKS,
            TetrominoType::O => O_BLOCKS,
            TetrominoType::S => S_BLOCKS,
            TetrominoType::T => T_BLOCKS,
            TetrominoType::Z => Z_BLOCKS,
        };
    }

    pub fn kick_offsets(&self) -> [[(i32, i32); 5]; 4] {
        return match *self {
            TetrominoType::I => I_KICK_OFFSETS,
            TetrominoType::J => JLSTZ_KICK_OFFSETS,
            TetrominoType::L => JLSTZ_KICK_OFFSETS,
            TetrominoType::O => O_KICK_OFFSETS,
            TetrominoType::S => JLSTZ_KICK_OFFSETS,
            TetrominoType::T => JLSTZ_KICK_OFFSETS,
            TetrominoType::Z => JLSTZ_KICK_OFFSETS,
        };
    }

    pub fn values() -> [TetrominoType; 7] {
        return [
            TetrominoType::I,
            TetrominoType::J,
            TetrominoType::L,
            TetrominoType::O,
            TetrominoType::S,
            TetrominoType::T,
            TetrominoType::Z];
    }
}