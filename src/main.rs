mod block;
mod piece;
mod queue;
mod grid;
mod sim;
mod game;
mod tetromino_data;

extern crate sdl2;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;
use sdl2::pixels;
use sim::TetrisSim;
use grid::SetPieceResult;
use sdl2::video::Window;
use sdl2::render::Canvas;
use block::Block;
use sdl2::render::BlendMode;
use piece::Tetromino;
use game::TetrisGame;

fn main() {
    let sdl = sdl2::init().unwrap();
    let video_subsystem = sdl.video().unwrap();
    let window = video_subsystem
        .window("tetrs", 640, 704)
        .resizable()
        .opengl()
//        .allow_highdpi()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas()
        .target_texture()
        .present_vsync()
        .build()
        .unwrap();

    canvas.set_blend_mode(BlendMode::Blend);

    let mut event_pump = sdl.event_pump().unwrap();

    let mut tetris_game = game::TetrisGame::new();

    'main: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => break 'main,
                Event::KeyDown { keycode: Some(key), .. } => {
                    match key {
                        Keycode::Left => { tetris_game.input.left.down_event(); }
                        Keycode::Right => { tetris_game.input.right.down_event(); }
                        Keycode::Down => { tetris_game.input.down.down_event(); }
                        Keycode::Space => {
                            let res = tetris_game.input.hard_drop.down_event();
                        }
                        Keycode::Up | Keycode::X => { tetris_game.input.rotate_cw.down_event(); }
                        Keycode::Z => { tetris_game.input.rotate_ccw.down_event(); }
                        Keycode::LShift => { tetris_game.input.hold.down_event(); }
                        Keycode::Escape => { break 'main; }
                        _ => {}
                    }
                }
                Event::KeyUp { keycode: Some(key), .. } => {
                    match key {
                        Keycode::Left => { tetris_game.input.left.up_event(); }
                        Keycode::Right => { tetris_game.input.right.up_event(); }
                        Keycode::Down => { tetris_game.input.down.up_event(); }
                        Keycode::Space => {
                            let res = tetris_game.input.hard_drop.up_event();
                        }
                        Keycode::Up | Keycode::X => { tetris_game.input.rotate_cw.up_event(); }
                        Keycode::Z => { tetris_game.input.rotate_ccw.up_event(); }
                        Keycode::LShift => { tetris_game.input.hold.up_event(); }
                        _ => {}
                    }
                }
                _ => {}
            }
        }

        tetris_game.tick();

        if !tetris_game.is_running {
            break 'main;
        }

        canvas.set_draw_color(pixels::Color::RGB(0, 0, 0));
        canvas.clear();

        // Draw grid
        for r in 0..22 {
            for c in 0..10 {
                draw_block(r, c, tetris_game.sim.grid.get_block(r, c), &mut canvas, 255);
            }
        }

        // Draw falling piece
        for b in tetris_game.sim.piece.iter() {
            draw_block(b.0, b.1, Block::Present(tetris_game.sim.piece.tetromino_type.color()), &mut canvas, 255);
        }

        // Draw ghost piece
        for b in tetris_game.sim.get_ghost().iter() {
            draw_block(b.0, b.1, Block::Present(tetris_game.sim.piece.tetromino_type.color()), &mut canvas, 64);
        }

        // Draw piece queue
        let n = 5;
        let queue = tetris_game.sim.piece_queue.preview(n);
        for i in 0..n {
            let piece_type = queue[i];
            set_color(&mut canvas, piece_type.color(), 255);
            canvas.fill_rect(Rect::new(320, (64+4) * i as i32, 64, 64));
        }

        // Draw held piece
        if tetris_game.sim.held_piece.is_some() {
            let held_type = tetris_game.sim.held_piece.unwrap();
            set_color(&mut canvas, held_type.color(), 255);
            canvas.fill_rect(Rect::new(320+64+4, 0, 64, 64));
        }

        canvas.present();
    }
}

fn set_color(canvas: &mut Canvas<Window>, abstract_color: u32, alpha: u8) {
    match abstract_color {
        0 => canvas.set_draw_color(pixels::Color::RGBA(0, 255, 255, alpha)),
        1 => canvas.set_draw_color(pixels::Color::RGBA(0, 0, 255, alpha)),
        2 => canvas.set_draw_color(pixels::Color::RGBA(255, 170, 0, alpha)),
        3 => canvas.set_draw_color(pixels::Color::RGBA(255, 255, 0, alpha)),
        4 => canvas.set_draw_color(pixels::Color::RGBA(0, 255, 0, alpha)),
        5 => canvas.set_draw_color(pixels::Color::RGBA(170, 0, 255, alpha)),
        6 => canvas.set_draw_color(pixels::Color::RGBA(255, 0, 0, alpha)),
        _ => canvas.set_draw_color(pixels::Color::RGBA(0, 0, 0, alpha)),
    };
}

fn draw_block(r: i32, c: i32, block: Block, canvas: &mut Canvas<Window>, alpha: u8) {
    match block {
        Block::Present(color) => {
            set_color(canvas, color, alpha);
            let block_rect = Rect::new((c as i32) * 32, ((22 - 1 - r) as i32) * 32, 32, 32);
            canvas.fill_rect(block_rect).unwrap();
            canvas.set_draw_color(pixels::Color::RGB(0, 0, 0));

            canvas.draw_rect(block_rect);
        }
        _ => {
            if (r + c) % 2 == 0 {
                canvas.set_draw_color(pixels::Color::RGB(32, 32, 32));
            } else {
                canvas.set_draw_color(pixels::Color::RGB(64, 64, 64));
            }
            canvas.fill_rect(Rect::new((c as i32) * 32, ((22 - 1 - r) as i32) * 32, 32, 32)).unwrap();
        }
    }
}