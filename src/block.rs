#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Block {
    Invalid,
    Empty,
    Present(u32),
}