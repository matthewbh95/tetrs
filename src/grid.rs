use piece::Tetromino;
use block::Block;
use tetromino_data::TetrominoType;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum SetPieceResult {
    None,
    Invalid,
    Single(usize),
    Double(usize, usize),
    Triple(usize, usize, usize),
    Tetris(usize, usize, usize, usize),
    TetrisPlus(usize),
}

impl SetPieceResult {
    pub fn plus(&self, new_line: usize) -> SetPieceResult {
        return match *self {
            SetPieceResult::None => SetPieceResult::Single(new_line),
            SetPieceResult::Single(l1) => SetPieceResult::Double(new_line, l1),
            SetPieceResult::Double(l1, l2) => SetPieceResult::Triple(new_line, l1, l2),
            SetPieceResult::Triple(l1, l2, l3) => SetPieceResult::Tetris(new_line, l1, l2, l3),
            SetPieceResult::Tetris(_, _, _, _) => SetPieceResult::TetrisPlus(5),
            SetPieceResult::TetrisPlus(n) => SetPieceResult::TetrisPlus(n + 1),
            _ => SetPieceResult::Invalid,
        };
    }
}

pub struct Grid {
    blocks: [[Block; 10]; 40],
}

impl Grid {
    pub fn new() -> Grid {
        return Grid { blocks: [[Block::Empty; 10]; 40] };
    }

    pub fn get_block(&self, r: i32, c: i32) -> Block {
        if r < 0 || r >= 40 || c < 0 || c >= 10 {
            return Block::Invalid;
        } else {
            return self.blocks[r as usize][c as usize];
        }
    }

    fn set_block(&mut self, r: i32, c: i32, new_block: Block) -> Block {
        let prev_block = self.get_block(r, c);
        if prev_block == Block::Invalid {
            return Block::Invalid;
        }
        self.blocks[r as usize][c as usize] = new_block;
        return prev_block;
    }

    fn is_row_full(&self, row: i32) -> bool {
        for col in 0..10 {
            match self.get_block(row, col) {
                Block::Present(_) => { continue; }
                _ => { return false; }
            }
        }
        return true;
    }

    fn clear_row(&mut self, row: i32) {
        for c in 0..10 {
            for r in (row as usize)..39 {
                self.blocks[r][c] = self.blocks[r + 1][c];
            }
            self.blocks[39][c] = Block::Empty;
        }
    }

    pub fn is_piece_valid(&self, piece: &Tetromino) -> bool {
        return piece.is_valid(&|pos| {
            return self.get_block(pos.0, pos.1) == Block::Empty;
        });
    }

    pub fn set_piece(&mut self, piece: &Tetromino) -> SetPieceResult {
        if !self.is_piece_valid(piece) {
            return SetPieceResult::Invalid;
        }

        let mut clear = SetPieceResult::None;

        let mut blocks: Vec<(i32, i32)> = piece.iter().collect();

        let mut row_affected = [false; 40];

        for (r, c) in blocks {
            self.set_block(r, c, Block::Present(piece.tetromino_type.color()));
            row_affected[r as usize] = true;
        }

        for r in (0..40).rev() {
            if row_affected[r] && self.is_row_full(r as i32) {
                self.clear_row(r as i32);
                clear = clear.plus(r);
            }
        }

        return clear;
    }
}