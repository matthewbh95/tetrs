use tetromino_data::TetrominoType;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Tetromino {
    pub row: i32,
    pub col: i32,
    pub orientation: usize,
    pub tetromino_type: TetrominoType,
}

impl Tetromino {
    pub fn move_to(&self, r: i32, c: i32) -> Tetromino {
        return Tetromino {
            row: r,
            col: c,
            orientation: self.orientation,
            tetromino_type: self.tetromino_type,
        };
    }

    pub fn translate(&self, dr: i32, dc: i32) -> Tetromino {
        return Tetromino {
            row: self.row + dr,
            col: self.col + dc,
            orientation: self.orientation,
            tetromino_type: self.tetromino_type,
        };
    }

    pub fn rotate_cw(&self, is_valid: &Fn((i32, i32)) -> bool) -> Tetromino {
        return self.rotate((self.orientation + 1) % 4, is_valid);
    }

    pub fn rotate_ccw(&self, is_valid: &Fn((i32, i32)) -> bool) -> Tetromino {
        return self.rotate((self.orientation + 3) % 4, is_valid);
    }

    fn rotate(&self, new_orientation: usize, is_empty: &Fn((i32, i32)) -> bool) -> Tetromino {
        let src = self.orientation;
        let dst = new_orientation % 4;
        for i in 0..5 {
            let src_offset = self.tetromino_type.kick_offsets()[src][i];
            let dst_offset = self.tetromino_type.kick_offsets()[dst][i];
            let delta = (src_offset.0 - dst_offset.0, src_offset.1 - dst_offset.1);
            let new_piece = Tetromino {
                row: self.row + delta.0,
                col: self.col + delta.1,
                orientation: dst,
                tetromino_type: self.tetromino_type,
            };
            if new_piece.is_valid(is_empty) {
                return new_piece;
            }
        }
        return *self;
    }

    pub fn is_valid(&self, is_valid_pos: &Fn((i32, i32)) -> bool) -> bool {
        return self.iter().all(|b| is_valid_pos(b));
    }

    fn rotate_point(local: (i32, i32), orientation: usize) -> (i32, i32) {
        return match orientation % 4 {
            0 => local,
            1 => (-local.1, local.0),
            2 => (-local.0, -local.1),
            3 => (local.1, -local.0),
            _ => local,
        };
    }

    fn get_block(&self, index: usize) -> Option<(i32, i32)> {
        if index >= 4 {
            return Option::None;
        }
        let local = Tetromino::rotate_point(self.tetromino_type.blocks()[index], self.orientation);
        return Option::from((local.0 + self.row, local.1 + self.col));
    }

    pub fn iter(&self) -> TetrominoBlockIterator {
        return TetrominoBlockIterator::new(self);
    }
}

pub struct TetrominoBlockIterator<'a> {
    tetromino: &'a Tetromino,
    block_index: usize,
}

impl<'a> TetrominoBlockIterator<'a> {
    fn new(tetromino: &Tetromino) -> TetrominoBlockIterator {
        return TetrominoBlockIterator {
            tetromino,
            block_index: 0,
        };
    }
}

impl<'a> Iterator for TetrominoBlockIterator<'a> {
    type Item = (i32, i32);

    fn next(&mut self) -> Option<(i32, i32)> {
        let next_block = self.tetromino.get_block(self.block_index);
        self.block_index += 1;
        return next_block;
    }
}