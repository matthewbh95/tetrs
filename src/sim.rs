use piece::Tetromino;
use grid::Grid;
use queue::PieceQueue;
use grid::SetPieceResult;
use tetromino_data::TetrominoType;
use block::Block;

pub struct TetrisSim {
    pub piece: Tetromino,
    pub held_piece: Option<TetrominoType>,
    pub piece_queue: PieceQueue,
    pub grid: Grid,
}

impl TetrisSim {
    pub fn new() -> TetrisSim {
        let mut queue = PieceQueue::new();
        let first_piece = queue.next().spawn(20, 4);
        return TetrisSim {
            piece: first_piece,
            held_piece: None,
            piece_queue: queue,
            grid: Grid::new(),
        };
    }

    pub fn move_piece_up(&mut self) -> bool {
        return self.move_piece(1, 0);
    }

    pub fn move_piece_down(&mut self) -> bool {
        return self.move_piece(-1, 0);
    }

    pub fn move_piece_left(&mut self) -> bool {
        return self.move_piece(0, -1);
    }

    pub fn move_piece_right(&mut self) -> bool{
        return self.move_piece(0, 1);
    }

    fn move_piece(&mut self, dr: i32, dc: i32) -> bool {
        let new_piece = self.piece.translate(dr, dc);
        if self.grid.is_piece_valid(&new_piece) {
            self.piece = new_piece;
            return true;
        }
        return false;
    }

    pub fn hold(& mut self) {
        if self.held_piece.is_none() {
            self.held_piece = Option::from(self.piece.tetromino_type);
            self.piece = self.piece_queue.next().spawn(20, 4);
        } else {
            let temp = self.held_piece.unwrap();
            self.held_piece = Option::from(self.piece.tetromino_type);
            self.piece = temp.spawn(20, 4);
        }
    }

    pub fn rotate_cw(&mut self) {
        self.piece = self.piece.rotate_cw(&|(r, c)| self.grid.get_block(r, c) == Block::Empty);
    }

    pub fn rotate_ccw(&mut self) {
        self.piece = self.piece.rotate_ccw(&|(r, c)| self.grid.get_block(r, c) == Block::Empty);
    }

    pub fn lock_piece(&mut self) -> SetPieceResult {
        let t_spin = self.is_t_spin();
        let result = self.grid.set_piece(&self.piece);
        match result {
            SetPieceResult::Single(_) => {println!("{} Single!", if t_spin {"T-Spin"} else {""})},
            SetPieceResult::Double(_, _) => {println!("{} Double!", if t_spin {"T-Spin"} else {""})},
            SetPieceResult::Triple(_, _, _) => {println!("{} Triple!", if t_spin {"T-Spin"} else {""})},
            SetPieceResult::Tetris(_, _, _, _) => {println!("{} Tetris!!!!", if t_spin {"T-Spin"} else {""})},
            _=>{},
        }
        self.piece = self.piece_queue.next().spawn(20, 4);
        if !self.grid.is_piece_valid(&self.piece) {
            return SetPieceResult::Invalid;
        }
        return result;
    }

    pub fn is_piece_on_ground(&self) -> bool {
        let test_piece = self.piece.translate(-1, 0);
        return !self.grid.is_piece_valid(&test_piece);
    }

    pub fn get_ghost(&self) -> Tetromino {
        let mut ghost = self.piece.translate(-1, 0);
        while self.grid.is_piece_valid(&ghost) {
            ghost = ghost.translate(-1, 0);
        }
        ghost = ghost.translate(1, 0);
        return ghost;
    }

    pub fn is_t_spin(&self) -> bool {
        if self.piece.tetromino_type == TetrominoType::T {
            let mut corner_count = 0;
            let tl = self.grid.get_block(self.piece.row + 1, self.piece.col - 1);
            corner_count += match tl { Block::Empty => 0, _=> 1};

            let tr = self.grid.get_block(self.piece.row + 1, self.piece.col + 1);
            corner_count += match tr { Block::Empty => 0, _=> 1};

            let bl = self.grid.get_block(self.piece.row - 1, self.piece.col - 1);
            corner_count += match bl { Block::Empty => 0, _=> 1};

            let br = self.grid.get_block(self.piece.row - 1, self.piece.col + 1);
            corner_count += match br { Block::Empty => 0, _=> 1};

            return corner_count >= 3;
        }
        return false;
    }
}