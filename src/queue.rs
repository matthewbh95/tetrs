extern crate rand;

use queue::rand::Rng;
use tetromino_data::TetrominoType;

pub struct PieceQueue {
    preview_buffer: Vec<TetrominoType>,
    piece_generator: Tetromino7Bag,
}

impl PieceQueue {
    pub fn new() -> PieceQueue {
        let mut generator = Tetromino7Bag::new();
        let mut buffer = Vec::new();
        return PieceQueue {
            preview_buffer: buffer,
            piece_generator: generator,
        };
    }

    fn ensure_capacity(&mut self, n: usize) {
        while self.preview_buffer.len() < n {
            self.preview_buffer.push(self.piece_generator.next());
        }
    }

    pub fn preview(&mut self, n: usize) -> &[TetrominoType] {
        self.ensure_capacity(n);
        return &self.preview_buffer[0..n];
    }

    pub fn next(&mut self) -> TetrominoType {
        self.ensure_capacity(1);
        return self.preview_buffer.remove(0);
    }
}

pub struct Tetromino7Bag {
    queue: [TetrominoType; 7],
    size: usize,
}

impl Tetromino7Bag {
    pub fn new() -> Tetromino7Bag {
        return Tetromino7Bag { queue: [TetrominoType::I; 7], size: 0 };
    }

    pub fn next(&mut self) -> TetrominoType {
        if self.size == 0 {
            let mut bag = TetrominoType::values();
            let mut rng = rand::thread_rng();
            rng.shuffle(&mut bag);
            self.queue.copy_from_slice(&bag);
            self.size = 7;
        }
        self.size -= 1;
        return self.queue[self.size];
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_7bag() {
        let mut generator = Tetromino7Bag::new();
        let mut buffer = [TetrominoType::I; 7];
        for _trial in 0..100 {
            for i in 0..7 {
                buffer[i] = generator.next();
            }
            for tetromino_type in TetrominoType::values().iter() {
                assert!(buffer.contains(&tetromino_type));
            }
        }
    }

    #[test]
    fn test_preview() {
        let mut queue = PieceQueue::new();
        for _trial in 0..100 {
            let mut preview = queue.preview(5).to_vec();
            for i in 0..5 {
                assert_eq!(preview[i], queue.next());
            }
        }
    }
}